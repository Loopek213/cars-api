stages:
  - build
  - test
  - deploy
  - testPostDeploy
  - publishingReports

variables:
  S3_BUCKET: cars-api-jar
  REGION: eu-central-1
  ARTIFACT_NAME: cars-api-v$CI_PIPELINE_IID.jar
  AWS_APP_NAME: cars-api

buildProject:
  stage: build
  image: openjdk:12-alpine
  script:
    - sed -i "s/CI_PIPELINE_IID/$CI_PIPELINE_IID/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_SHORT_SHA/$CI_COMMIT_SHORT_SHA/" ./src/main/resources/application.yml
    - sed -i "s/CI_COMMIT_BRANCH/$CI_COMMIT_BRANCH/" ./src/main/resources/application.yml
    - ./gradlew build
    - mv ./build/libs/cars-api.jar ./build/libs/$ARTIFACT_NAME
  artifacts:
    paths:
      - ./build/libs/

codeQualityTest:
  stage: test
  image: openjdk:12-alpine
  script:
    - ./gradlew pmdMain pmdTest
  artifacts:
    when: always
    paths:
      - ./build/reports/pmd

smokeTest:
  stage: test
  image: openjdk:12-alpine
  before_script:
    - apk --no-cache add curl
  script:
    - java -jar ./build/libs/$ARTIFACT_NAME &
    - sleep 20
    - curl http://localhost:5000/actuator/health | grep -q "UP"

unitTest:
  stage: test
  image: openjdk:12-alpine
  script:
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - ./build/reports/tests
    reports:
      junit: build/test-results/test/*.xml

deployAWS:
  stage: deploy
  image:
    name: banst/awscli
    entrypoint: [""]
  before_script:
    - apk --no-cache add curl
    - apk --no-cache add jq
  script:
    - aws configure set region $REGION
    - aws s3 cp ./build/libs/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    - aws elasticbeanstalk create-application-version --application-name $AWS_APP_NAME --version-label $CI_PIPELINE_IID
      --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    - URLNAME=$( aws elasticbeanstalk update-environment --application-name $AWS_APP_NAME
      --environment-name "CarsApi-env" --version-label $CI_PIPELINE_IID | jq '.CNAME' --raw-output)
    - sleep 25
    - curl http://$URLNAME/actuator/info | grep $CI_PIPELINE_IID
    - curl http://$URLNAME/actuator/health | grep "UP"

apiTest:
  stage: testPostDeploy
  image:
    name: vdespa/newman
    entrypoint: [""]
  script:
    - newman --version
    - newman run "./src/test/postman collection/Cars API.postman_collection.json"
      --environment "./src/test/postman collection/Production.postman_environment.json"
      --reporters cli,htmlextra,junit --reporter-htmlextra-export "newman/report.html"
      --reporter-junit-export "newman/report.xml"
  artifacts:
    when: always
    paths:
      - ./newman
    reports:
      junit: ./newman/report.xml

pages:
  stage: publishingReports
  script:
    - mkdir public
    - cp newman/report.html public/report.html
  artifacts:
    paths:
      - ./public